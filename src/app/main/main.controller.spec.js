(function() {
  'use strict';

  describe('controllers', function(){

    beforeEach(module('currency'));
	    it('should define more than 5 denominations', inject(function($controller) {
	      var vm = $controller('MainController');

	      expect(angular.isArray(vm.coins)).toBeTruthy();
	      expect(vm.coins.length > 5).toBeTruthy();
	    }));

  });
})();
