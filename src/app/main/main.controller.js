(function() {
  'use strict';

  angular
    .module('currency')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, toastr) {
    $scope.currency = 'Amount';

    // greedy calc
    //TODO: revisit calc for performance reasons. Explore options around dynamic programming
    var calculate = function(amount) {
      console.log('amount: '+ amount);
      var changeRequired = [], t = 0;
      //TODO: remove hardcoded values
      [200, 100, 50, 20, 2, 1].forEach(function(coin) {
        var update = 0;
        while (t + coin <= amount) {
          changeRequired.push(coin);
          update++;
          $scope['coin'+coin] = update;
          t += coin;
        }
      });
      return changeRequired;
    };

    //TODO: this is messy.. needs cleanup :-/
    function resetVals() {
      $scope.coin1 = 0, $scope.coin2 = 0, $scope.coin20 = 0, $scope.coin50 = 0, $scope.coin100 = 0, $scope.coin200 = 0;
    }

    //Check format of input to determine if value is in pences or pounds
    $scope.checkFormat = function(total){
      var decimal =  /^[-+]?[0-9]+\./;
      if(decimal.test(total)){
        $scope.currency = '&pound';
        $scope.checkedTotal = total * 100;
      }else if(total=='') {
        $scope.currency = 'Amount';
        resetVals();
      }else{
        $scope.currency = 'pence';
        $scope.checkedTotal = total;
      }
    }

    //notification message
    function showToastr() {
      toastr.info('It took ' + $scope.totalTime + ' ms to calculate change.');
    }

    $scope.calcChange = function(checkedTotal){
      resetVals();
      var checkedTotal = $scope.checkedTotal;
      var a = performance.now();
      $scope.change = calculate(parseInt(checkedTotal, 10)).map(function(coin) { return coin+', ' }).join(' ');
      var b = performance.now();
      $scope.totalTime = b-a;
      showToastr();
      console.log('It took ' + $scope.totalTime + ' ms to calculate change.');
    }




    //TODO: Place in external json to pull in data. Use this data rather than hardcoded values.
    $scope.coins = [
      {
        name: '2 pounds',
        id: 'coin200',
        value: '200',
        img: '/assets/images/2pounds.png'
      },
      {
        name: '1 pound',
        id: 'coin100',
        value: '100',
        img: '/assets/images/1pound.png'
      },
      {
        name: '50 pence',
        id: 'coin50',
        value: '50',
        img: '/assets/images/50pence.png'
      },
      {
        name: '20 pence',
        id: 'coin20',
        value: '20',
        img: '/assets/images/20pence.png'
      },
      {
        name: '2 pence',
        id: 'coin2',
        value: '2',
        img: '/assets/images/2pence.png'
      },
      {
        name: '1 penny',
        id: 'coin1',
        value: '1',
        img: '/assets/images/1penny.png'
      },
    ];
  }

})();
