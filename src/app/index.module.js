(function() {
  'use strict';

  angular
    .module('currency', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial', 'ngGeolocation']);

})();
