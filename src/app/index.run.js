(function() {
  'use strict';

  angular
    .module('currency')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
