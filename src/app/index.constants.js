/* global moment:false */
(function() {
  'use strict';

  angular
    .module('currency')
    .constant('toastr', toastr)
    .constant('moment', moment);

})();
